﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.Network
{
    public partial class NetworkClient
    {
        // Delegates.
        public delegate void ConnectedToServerEvent();
        public delegate void PingEvent( string content );
        public delegate void MeJoinedChannelEvent( string channel );
        public delegate void MeLeftChannelEvent( string channel );
        public delegate void UserJoinedChannelEvent( string channel, string user );
        public delegate void UserLeftChannelEvent( string channel, string user );
        public delegate void ChannelMessageEvent( string channel, string user, string message );
        
        // Events.
        public event ConnectedToServerEvent ConnectedToServer;
        public event PingEvent Ping;
        public event MeJoinedChannelEvent MeJoinedChannel;
        public event MeLeftChannelEvent MeLeftChannel;
        public event UserJoinedChannelEvent UserJoinedChannel;
        public event UserLeftChannelEvent UserLeftChannel;
        public event ChannelMessageEvent ChannelMessage;

    }
}
