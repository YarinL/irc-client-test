﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.Network
{
    public partial class NetworkClient : NetworkClientBase
    {
        public readonly Form1 _clientForm;
        public readonly Form2 _debugWindow;
        private readonly StringBuilder _buffer = new StringBuilder(1024);
        //private readonly Dictionary<string, MethodInfo> _packetMethods = new Dictionary<string, MethodInfo>();
        private readonly string _myNick;
        private string[] _packetHeaders = { "001",
                                            "353",
                                            "433",
                                            "JOIN",
                                            "MODE",
                                            "NICK",
                                            "NOTICE",
                                            "PRIVMSG",
                                            "PART",
                                            "QUIT",
                                            "PING"};

        public NetworkClient(Form1 clientForm, Form2 debugForm)
        {
            _clientForm = clientForm;
            _debugWindow = debugForm;
            if( string.IsNullOrWhiteSpace( _myNick = clientForm.GetNick ) )
                throw new ArgumentException("Client nick name cannot be empty.");
        }
  
        public void OnJoinChannel(string channelName)
        {
            if( string.IsNullOrWhiteSpace( channelName ) )
                throw new ArgumentException( "Channel name must not be empty." );

            if( channelName[ 0 ] != '#' ) // we dont support 'local' channels like &test (see IRC protocol)
                channelName = channelName.Insert( 0, "#" );

            SendFormat( "JOIN {0}\r\n", channelName );
            _debugWindow.DebugLog( "JOIN {0}", channelName );
        }

        protected override void OnDispose()
        {
    
        }

        protected override void OnConnected()
        {
            var address = Socket.RemoteEndPoint.ToString().Split(':');
            NetworkLogger.LogConnected(address[0], int.Parse(address[1]));

            Send( string.Format( "NICK {0}\r\nUSER {0} 0 * :{0}\r\n", _myNick ) );
            _debugWindow.DebugLog( "NICK {0}\r\n", _myNick );
        }

        protected override void OnDisconnected()
        {
            NetworkLogger.LogDisconnected();
        }

        protected override void OnData(byte[] buffer, int length)
        {

            // Encoding bytes to string.
            
            var data = _buffer.ToString() + Encoding.GetEncoding(1252).GetString(buffer, 0, length);
            NetworkLogger.LogIncoming(data); // Logs.
            _debugWindow.DebugLog( data );  // Debug Log.
            _buffer.Clear(); // Clear buffer.

            int pos;

            // Removing '\n' from string.
            while((pos = data.IndexOf('\n')) !=  -1)
            {
                var packet = data.Substring(0, pos);
                data = data.Substring(pos + 1);

                if(packet.Length > 0)
                {
                    // Removing '\r' from string.
                    if(packet.EndsWith("\r"))
                    {
                        packet = packet.Substring( 0, packet.Length - 1 );
                        if (packet.Length == 0)
                            continue;
                    }

                    //log packet.
                    System.IO.File.AppendAllText( "datalog.txt", string.Format( "[{0}]\r\n{1}\r\n\r\n", DateTime.Now.ToString( "HH:mm:ss" ), packet ) );

                    string host = string.Empty;

                    // Look for ':' in the string.
                    if(packet[0] == ':')
                    {
                        // Looking for spaces in string.
                         if((pos = packet.IndexOf(' ')) == -1)
                         {
                             System.IO.File.AppendAllText( "datalog.txt", "[WARNING] DISCARDED (No content in packet)\r\n\r\n" );
                             continue;
                         }

                        // Pass the string to host till the first space (without the the ':')
                        // and pass the rest of the string to packet
                        host = packet.Substring( 1, pos - 1 );
                        packet = packet.Substring( pos + 1 );

                        // Checking if packet is legit.
                        if( packet.Length == 0 )
                            continue;
                    }

                    string content = string.Empty;

                    //Look for the next space
                    if((pos = packet.IndexOf(' ')) != -1)
                    {
                        content = packet.Substring( pos + 1 );
                        packet = packet.Substring( 0, pos );
                    }
                    
                    if( !PreprocessPacket( host, packet, content ) )
                    {
                        // do something ...
                        System.IO.File.AppendAllText( "datalog.txt", "[WARNING] Preprocess packet failed\r\n\r\n" );
                    }

                    if( data.Length > 0 )
                        _buffer.Append( data );
                }
            }
        }

        private bool PreprocessPacket( string host, string header, string content )
        {
            var x_nick = string.Empty;
            var x_account = string.Empty;
            var x_host = string.Empty;
            int pos;

            var host_x = host;
            if( ( pos = host_x.IndexOf( '@' ) ) != -1 ) // strip away hostname
            {
                x_host = host_x.Substring( pos + 1 );
                host_x = host_x.Substring( 0, pos );
            }

            if( ( pos = host_x.IndexOf( '!' ) ) != -1 ) // strip away account name
            {
                x_account = host_x.Substring( pos + 1 );
                host_x = host_x.Substring( 0, pos );
            }

            if( x_host.Length == 0 )
                x_host = host_x;
            else
                x_nick = host_x;

            //header = header.ToLower();

            if(!_packetHeaders.Contains(header))
            {
                System.IO.File.AppendAllText( "datalog.txt", string.Format( "Unknown packet header: {0}\r\n", header ) );
                return false;
            }
            

            PacketInvocationData _packetInvocationData = new PacketInvocationData( x_host, x_account, x_nick, content );

            switch(header)
            {
                case "001": On001( _packetInvocationData ); break;
                case "353": On353( _packetInvocationData ); break;
                case "433": On433( _packetInvocationData ); break;
                case "JOIN": OnJoin( _packetInvocationData ); break;
                case "MODE": OnMode( _packetInvocationData ); break;
                case "NICK": OnNick( _packetInvocationData   ); break;
                case "NOTICE": OnNotice( _packetInvocationData ); break;
                case "PRIVMSG": OnPrivMsg( _packetInvocationData ); break;
                case "PART": OnPart( _packetInvocationData ); break;
                case "QUIT": OnQuit( _packetInvocationData ); break;
                case "PING": OnPing( _packetInvocationData ); break;
                default: throw new ArgumentException( "Unknown packet header." );

            }

            return true;
        }

        public int Send( string data )
        {
            NetworkLogger.LogOutgoing( data );
            var binary = Encoding.GetEncoding( 1252 ).GetBytes( data );
            return Send( binary, binary.Length );
        }

        public int SendFormat( string format, params object[] args )
        {
            return Send( string.Format( format, args ) );
        }

        public override void Process()
        {
            base.Process(); // must be called

            // ...
        }
    }
}
