﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.Network
{
    public partial class NetworkClient
    {

        public void OnConnectedToServer()
        {
            _clientForm._nextJoinTick = Environment.TickCount + 1000;
            _clientForm._isFullyConnected = true;
        }

        public void OnPing( string content)
        {
            SendFormat( "PONG {0}\r\n", content );
            _debugWindow.DebugLog( "PONG {0}\r\n", content );
        }
        public void OnUserJoinChannel(string channel, string user)
        {
            _clientForm.lb_UserList.Items.Add( user );  
        }
        public void OnUserLeftChannel( string channel, string user )
        {
            _clientForm.lb_UserList.Items.Remove( user );
        }

        public void OnChannelMessage(string channel, string user, string message)
        {
            _clientForm.AddChatMessage( DateTime.Now, channel, user, message, true );
        }
    }
}
