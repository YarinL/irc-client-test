﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.Network
{
    public partial class NetworkClient
    {
        protected virtual void On001(PacketInvocationData pid)
        {
            if( ConnectedToServer != null )
                ConnectedToServer();
            
        }
        protected virtual void On353( PacketInvocationData pid )
        {

        }
        protected virtual void On433( PacketInvocationData pid )
        {

        }

        protected virtual void OnJoin( PacketInvocationData pid )
        {

        }
        protected virtual void OnMode( PacketInvocationData pid )
        {

        }

        protected virtual void OnNick( PacketInvocationData pid )
        {

        }

        protected virtual void OnNotice( PacketInvocationData pid )
        {

        }

        protected virtual void OnPrivMsg( PacketInvocationData pid )
        {

        }

        protected virtual void OnPart( PacketInvocationData pid )
        {

        }

         protected virtual void OnQuit( PacketInvocationData pid )
        {

        }

         protected virtual void OnPing( PacketInvocationData pid )
        {
            _clientForm._PongSent = false;
            if( Ping != null )
                Ping( pid.Content );
        }


    }

#region PacketAttribute
    [AttributeUsage(AttributeTargets.Method)]
    public class PacketAttribute : Attribute
    {
        public string Header {get; private set; }

        public PacketAttribute(string header)
        {
            Header = header.ToLower();
        }
    }

    public class PacketInvocationData
    {
        public string Host { get; private set; }
        public string Account { get; private set; }
        public string Nick { get; private set; }
        public string Content { get; private set; }
        public PacketInvocationData(string host, string account, string nick, string content)
        {
            Host = host;
            Account = account;
            Nick = nick;
            Content = content;
        }
    }
#endregion
}
