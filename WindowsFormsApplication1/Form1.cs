﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.Network;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        private readonly NetworkClient _client;
        private Form2 _debugWindow = new Form2();

        protected bool IsConnected = false;
        public bool _isJoiningChannel = false;
        public bool _isFullyConnected = false;
        public int _nextJoinTick = 0;
        public bool _PongSent = false;

        public string GetNick { get { return txtNick.Text; } }
        public Form1()
        {
            InitializeComponent();
            _client = new NetworkClient( this, _debugWindow );
            
            _debugWindow.Show();

            
                                                            
            new Timer() { Interval = 100, Enabled = true }.Tick += ( sender, e ) =>
                {
                    ( sender as Timer ).Enabled = false;
                    _client.Process();

                    var tick = Environment.TickCount;

                    //Events
                    if(! _PongSent )
                        _client.Ping += _client.OnPing;
                    _client.ConnectedToServer += _client.OnConnectedToServer;
                    

                    //Checking joining 
                    if(tick >= _nextJoinTick &&
                        _isFullyConnected &&
                        !_isJoiningChannel)
                    {
                        _isJoiningChannel = true;
                        _client.OnJoinChannel( txtChannel.Text );
                    }

                        ( sender as Timer ).Enabled = true;

                };
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if(!IsConnected)
            {
                btnConnect.Text = "...";
                txtServer.Enabled = false;
                txtNick.Enabled = false;
                txtChannel.Enabled = false;

                System.Threading.Thread.Sleep( 3000 );

                if( string.IsNullOrWhiteSpace( txtNick.Text ) &&
                string.IsNullOrWhiteSpace( txtServer.Text ) &&
                string.IsNullOrWhiteSpace( txtChannel.Text ) )
                {
                    btnConnect.Text = "Connect";
                    txtServer.Enabled = true;
                    txtNick.Enabled = true;
                    txtChannel.Enabled = true;

                    throw new ArgumentException( "Empty Fields." );
                }

                _client.Connect( txtServer.Text, 6667 );
                IsConnected = true;
                btnConnect.Text = "Disconnect";
            }
            else
            {
                _client.Close();
                IsConnected = true;

                btnConnect.Text = "Connect";

                txtServer.Enabled = true;
                txtNick.Enabled = true;
                txtChannel.Enabled = true;
            }
               
        }

        public void AddChatMessage(DateTime date, string channel, string user, string message, bool addSender)
        {
            RichTextBox chat = null;
        }
    }
}
